import { Home } from "./components/Home";
import { User } from "./components/User";

const AppRoutes = [
  {
    index: true,
    element: <Home />,
  },
  {
    path: "/user/:id",
    element: <User />,
  },
];

export default AppRoutes;
