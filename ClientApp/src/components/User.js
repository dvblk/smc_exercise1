import React, { Fragment } from "react";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { set, omit } from "lodash-es";
import { XSquare, Edit } from "react-feather";

export function User() {
  const { id } = useParams();

  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function fetchUser(id) {
      try {
        const response = await fetch(`api/users/${id}`);
        const data = await response.json();
        setUser(data);
        setLoading(false);
      } catch (e) {
        console.log(e);
      }
    }

    if (id !== user.id) fetchUser(id);
  }, [loading, id]);

  const contents = loading ? (
    <p>
      <em>Loading...</em>
    </p>
  ) : (
    <Details {...{ user, setUser }} />
  );

  return <div>{contents}</div>;
}

const Todos = ({ todos, handleAddTodo, handleUpdateTodo, handleDeleteTodo }) => {
  const [todoTitle, setTodoTitle] = useState('')
  return (
    <div className="card col mx-2" style={{ borderRadius: "1rem" }}>
      <div className="card-body p-2 pt-3">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <form className="d-flex justify-content-center align-items-center mb-2" onSubmit={(e) => handleAddTodo(e,todoTitle)}>
            <div className="input-group mb-2">
              <input
                type="text"
                value={todoTitle}
                onChange={(e)=>setTodoTitle(e.target.value)}
                className="form-control"
                placeholder="Add Todo"
                aria-label="Add Todo"
                aria-describedby="basic-addon2"
              />
              <div className="input-group-append">
                <button className="btn btn-outline-secondary" type="submit">
                  Add
                </button>
              </div>
            </div>
          </form>
          <ul
            className="list-group mb-0 border"
            style={{
              maxHeight: "45vh",
              overflowY: "scroll",
              overflowX: "hidden",
            }}
          >
            {todos.sort((a,b)=> b.id - a.id).map((todo, key) => {
              return <Todo {...{ todo, handleUpdateTodo, handleDeleteTodo, key }} />;
            })}
          </ul>
        </div>
      </div>
    </div>
  );
};

const Todo = ({ todo, handleUpdateTodo, handleDeleteTodo }) => {
  return (
    <li className="list-group-item d-flex justify-content-between align-items-center border-start-0 border-top-0 border-end-0 border-bottom rounded-0 mb-2">
      <div className="d-flex align-items-center">
        <input
          className="form-check-input me-2"
          type="checkbox"
          checked={todo.completed}
          onChange={() => handleUpdateTodo(todo)}
          aria-label="..."
        />
        {todo.title}
      </div>
      <XSquare
        onClick={() => handleDeleteTodo(todo.id)}
        className="text-danger"
        style={{ cursor: "pointer" }}
      />
    </li>
  );
};

const Details = ({ user, setUser }) => {
  const [isPostsShowing, setIsPostsShowing] = useState(true);
  const [editingPath, setEditingPath] = useState("");
  const toggleIsPostsShowing = () => setIsPostsShowing(!isPostsShowing);

  const handleSubmit = async (e) => {
    e.preventDefault();

    let updatedUser = Object.assign({}, user);
    set(updatedUser, editingPath, e.target.value);

    let updatedUserBody = omit(updatedUser, ["todos", "posts", "albums"]);

    // TODO: Entity doesn't send these values, why not?
    updatedUserBody.addressId = user.address.id;
    updatedUserBody.companyId = user.company.id;

    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(updatedUserBody),
    };

    try {
      await fetch(`/api/users/${user.id}`, requestOptions);

      setEditingPath("");
      setUser(updatedUser);
    } catch (e) {
      console.log(e);
    }
  };

  const handleDeleteTodo = async (todoId) => {
    try {
      await fetch(`/api/todos/${todoId}`, {
        method: "DELETE",
      });

      const updatedUser = Object.assign({}, user, {
        todos: user.todos.filter(({ id }) => id !== todoId),
      });
      setUser(updatedUser);
    } catch (e) {
      console.log(e);
    }
  };

  const handleAddTodo = async (e, todoTitle) => {
    e.preventDefault();

    const todo = { id: 0, title: todoTitle, completed: false, userId: user.id }

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(todo),
    };

    try {
      const res = await fetch('/api/todos/', requestOptions);
      const json = await res.json()
      console.log(json)

      const updatedUser = Object.assign({}, user, {
        todos: [ json, ...user.todos ],
      });

      setUser(updatedUser);
    } catch (e) {
      console.log(e);
    }
  };

  const handleUpdateTodo = async (todo) => {
    const updatedTodo = { ...todo, completed: !todo.completed }
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(updatedTodo),
    };

    try {
      await fetch(`/api/todos/${todo.id}`, requestOptions);

      const updatedUser = Object.assign({}, user, {
        todos: [ updatedTodo, ...user.todos.filter(({ id }) => id !== todo.id) ],
      });

      setUser(updatedUser);
    } catch (e) {
      console.log(e);
    }
  };

  const personalDetails = [
    { label: "Full Name", value: user.name, path: "name" },
    { label: "Email", value: user.email, path: "email" },
    { label: "Phone", value: user.phone, path: "phone" },
    { label: "Website", value: user.website, path: "website" },
  ];

  const {
    address = {},
    company = {},
    albums = [],
    posts = [],
    todos = [],
  } = user;

  const addressDetails = [
    { label: "Street", value: address.street, path: "address.street" },
    { label: "Suite", value: address.suite, path: "address.suite" },
    { label: "City", value: address.city, path: "address.city" },
    { label: "Zipcode", value: address.zipcode, path: "address.zipcode" },
  ];

  const companyDetails = [
    { label: "Company Name", value: company.name, path: "company.name" },
    {
      label: "Catch Phrase",
      value: company.catchPhrase,
      path: "company.catchPhrase",
    },
    { label: "B.S.", value: company.bs, path: "company.bs" },
  ];

  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-4">
          <LeftColumnSection
            {...{
              list: personalDetails,
              editingPath,
              setEditingPath,
              handleSubmit,
            }}
          />
          <LeftColumnSection
            {...{
              list: addressDetails,
              editingPath,
              setEditingPath,
              handleSubmit,
            }}
          />
          <LeftColumnSection
            {...{
              list: companyDetails,
              editingPath,
              setEditingPath,
              handleSubmit,
            }}
          />
        </div>
        <div className="col-lg-8">
          <div className="container">
            <div className="row mb-4">
              <h4>{user.name}'s Todos</h4>
              <Todos {...{ todos, handleAddTodo, handleUpdateTodo, handleDeleteTodo }} />
            </div>
            <div className="row mb-4">
              <h4>{user.name}'s Posts</h4>
              <div className="accordion">
                <div className="accordion-item">
                  <h2
                    className="accordion-header p-0"
                    onClick={() => toggleIsPostsShowing()}
                    id="headingOne"
                  >
                    <button
                      className="accordion-button p-1"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    ></button>
                  </h2>
                  <div
                    className={`accordion-collapse collapse ${
                      isPostsShowing ? "show" : ""
                    }`}
                    style={{
                      maxHeight: "75vh",
                      overflowY: "scroll",
                      overflowX: "hidden",
                    }}
                  >
                    {posts.map((post, key) => {
                      return <Post {...{ post, key }} />;
                    })}
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <h4>{user.name}'s Albums</h4>
              <Gallery {...{ albums }} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const Detail = ({
  label,
  value,
  path,
  editingPath,
  setEditingPath,
  handleSubmit,
}) => {
  let valueContent = <div />;
  switch (label) {
    case "Email":
      valueContent = (
        <a href={`mailto:${value}`} className="text-muted mb-0">
          {value}
        </a>
      );
      break;
    case "Phone":
      valueContent = (
        <a href={`tel:+${value}`} className="text-muted mb-0">
          {value}
        </a>
      );
      break;
    default:
      valueContent = <p className="text-muted mb-0">{value}</p>;
      break;
  }

  const onKeyDown = (e) => {
    if (e.code === "Enter") {
      handleSubmit(e);
    }
  };

  return (
    <div
      className="row"
      style={{ cursor: "pointer" }}
      onClick={() => setEditingPath(path)}
    >
      <div className="col-sm-5">
        <p className="mb-0">{label}</p>
      </div>
      <div className="col-sm-7">
        {path === editingPath ? (
          <input type="text" defaultValue={value} onKeyDown={onKeyDown} />
        ) : (
          <span className="d-flex flex-row">
            <span className="me-2">
              <Edit size="15px" />
            </span>
            <span>{valueContent}</span>{" "}
          </span>
        )}
      </div>
    </div>
  );
};

const Post = ({ post }) => {
  return (
    <div className="card m-2">
      <div className="card-body">
        <h5 className="card-title">{post.title}</h5>
        <p className="card-text">{post.body}</p>
      </div>
    </div>
  );
};

const LeftColumnSection = ({
  list,
  editingPath,
  setEditingPath,
  handleSubmit,
}) => {
  return (
    <div className="row">
      <div className="card mb-4">
        <div className="card-body">
          {list.map((attrs, i) => {
            if (i === list.length - 1)
              return (
                <Detail
                  {...{
                    ...attrs,
                    editingPath,
                    setEditingPath,
                    handleSubmit,
                    key: i,
                  }}
                />
              );
            return (
              <Fragment key={i}>
                <Detail
                  {...{ ...attrs, editingPath, setEditingPath, handleSubmit }}
                />
                <hr />
              </Fragment>
            );
          })}
        </div>
      </div>
    </div>
  );
};

const Gallery = ({ albums }) => {
  const [selected, setSelected] = useState(albums[0].id);
  const [photosByAlbumId, setPhotosByAlbumId] = useState({});
  const toggleSelected = (albumId) => {
    if (selected === albumId) return setSelected(0);
    return setSelected(albumId);
  };

  useEffect(() => {
    async function fetchPhotos(albumId) {
      const response = await fetch(
        `https://jsonplaceholder.typicode.com/album/${albumId}/photos`
      );
      const data = await response.json();

      setPhotosByAlbumId({
        ...photosByAlbumId,
        [albumId]: data,
      });
    }

    if (photosByAlbumId[selected] === undefined) {
      fetchPhotos(selected);
    }
  }, [selected]);

  return (
    <div className="accordion" id="accordionExample">
      {albums.map((album, key) => {
        return (
          <Album
            {...{
              album,
              selected,
              toggleSelected,
              photos: photosByAlbumId[album.id],
              key,
            }}
          />
        );
      })}
    </div>
  );
};

const Album = ({ album, selected, toggleSelected, photos = [] }) => {
  return (
    <div className="accordion-item">
      <h2
        className="accordion-header"
        onClick={() => toggleSelected(album.id)}
        id="headingOne"
      >
        <button
          className="accordion-button"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#collapseOne"
          aria-expanded="true"
          aria-controls="collapseOne"
        >
          {album.title}
        </button>
      </h2>
      <div
        id="collapseOne"
        className={`accordion-collapse collapse ${
          selected === album.id ? "show" : ""
        }`}
        aria-labelledby="headingOne"
        data-bs-parent="#accordionExample"
      >
        <div
          className="accordion-body d-flex flex-row flex-wrap"
          style={{ maxHeight: "75vh", overflowY: "scroll" }}
        >
          {photos.length
            ? photos.map((photo, key) => {
                return (
                  <div
                    className="card m-2"
                    style={{ width: "14rem" }}
                    {...{ key }}
                  >
                    <img className="card-img-top" src={photo.thumbnailUrl} />
                    <div className="card-body">
                      <div className="card-text">{photo.title}</div>
                    </div>
                  </div>
                );
              })
            : "Loading..."}
        </div>
      </div>
    </div>
  );
};
