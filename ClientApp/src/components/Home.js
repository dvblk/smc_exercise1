import React from 'react';
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from 'react';

const UserTable = ({ users }) => {
    const navigate = useNavigate()
    return (
        <table className='table table-striped table-hover' aria-labelledby="tabelLabel">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Website</th>
                </tr>
            </thead>
            <tbody style={{cursor:'pointer'}}>
                {users.map(user =>
                    <tr key={user.id} onClick={() => navigate(`/user/${user.id}`)}>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                        <td>{user.website}</td>
                    </tr>
                )}
            </tbody>
        </table>
    );
}

export function Home() {
    const [users, setUsers] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function fetchUsers() {
            try {
                const response = await fetch('api/users/');
                const data = await response.json();
                setUsers(data);
                setLoading(false);
            } catch (e) {
                console.log(e)
            }
        }
        fetchUsers()

    }, [loading])

    const contents = loading
        ? <p><em>Loading...</em></p>
        : <UserTable users={users} />;

    return <div>
        <h1 id="tabelLabel" >Users</h1>
        <p>User data for this exercise.</p>
        {contents}
    </div>
}