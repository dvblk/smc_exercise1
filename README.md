SMC Exercise 1
============

#### Overview

This is a database first approach to building a react SPA with a C# Entity Framework Model/Controller backend, with automated migrations and seeding added later.

User entities are associated with posts, todos, and albums - photos are associated to albums and fetched on the frontend.

Users are listed on the home page and clicking a user displays associated data. User details and Todos can be modified without refreshing the page. 

#### Installation

`exercise-1.exe` can be found in the `distributed` directory (found as a release file [here](https://gitlab.com/dvblk/smc_exercise1/-/releases/main)) and can be run as-is as a server. Navigating the browser to https://localhost:5001 will bring up the service.

#### Build

This project was largely written and compiled in Visual Studio 2022. Opening this directory\'s solution file (`.sln`) with VS 2022 and potentially earlier versions will allow you to use debug, build and publish this project.

Once the project is loaded in the editor, right click the `Client App` directory and open in terminal, and run `npm install`

###### Footnotes
Due to some frontloaded learning of the C# toolset, there are improvements that I would have typically made to the frontend: 
- Text fields are not validated by the frontend.
- CRUD is fully implemented throughout the application with various things, but it has yet the option to: delete, edit and create posts/albums; delete and create users.
- Listing companies, albums, addresses
