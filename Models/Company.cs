﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace exercise_1.Models
{
    public partial class Company
    {
        public Company()
        {
            Users = new HashSet<User>();
        }
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? CatchPhrase { get; set; }
        public string? Bs { get; set; }
        [JsonIgnore]
        public virtual ICollection<User> Users { get; set; }
    }
}
