﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace exercise_1.Models
{
    public partial class User
    {
        public User()
        {
            Albums = new HashSet<Album>();
            Posts = new HashSet<Post>();
            Todos = new HashSet<Todo>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Email { get; set; } = null!;
        public int AddressId { get; set; }
        public string? Phone { get; set; }
        public string? Website { get; set; }
        public int? CompanyId { get; set; }
        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; } = null!;
        [ForeignKey("CompanyId")]
        public virtual Company? Company { get; set; }
        public virtual ICollection<Album> Albums { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Todo> Todos { get; set; }
    }
}
