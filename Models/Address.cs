﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace exercise_1.Models
{
    public partial class Address
    {
        public Address()
        {
            Users = new HashSet<User>();
        }
        public int Id { get; set; }
        public string Street { get; set; } = null!;
        public string? Suite { get; set; }
        public string City { get; set; } = null!;
        public string Zipcode { get; set; } = null!;
        // TODO: implement lat/long
        [JsonIgnore]
        public decimal? Latitude { get; set; }
        [JsonIgnore]
        public decimal? Longitude { get; set; }
        [JsonIgnore]
        public virtual ICollection<User> Users { get; set; }
    }
}
