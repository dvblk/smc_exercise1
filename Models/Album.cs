﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace exercise_1.Models
{
    public partial class Album
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; } = null!;
        [JsonIgnore]
        public virtual User User { get; set; } = null!;
    }
}
