﻿using exercise_1.Controllers;
using exercise_1.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddDbContextFactory<Exercise1Context>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("Exercise1"));
});

var app = builder.Build();

var scope = app.Services.CreateScope();
var context = scope.ServiceProvider.GetRequiredService<Exercise1Context>();

// Migrate and seed, even when published, for demonstration purposes.
context.Database.Migrate();
await EnsureSeeded(context);

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.MapControllerRoute(
    name: "default",
    pattern: "api/{controller}/{id?}");

app.MapFallbackToFile("index.html");;

app.Run();

static async Task EnsureSeeded(Exercise1Context context)
{
    if (!context.Users.Any())
    {
        using HttpClient client = new()
        {
            BaseAddress = new Uri("https://jsonplaceholder.typicode.com")
        };

        // Get the user information
        List<User>? users = await client.GetFromJsonAsync<List<User>>("users/");
        List<Post>? posts = await client.GetFromJsonAsync<List<Post>>("posts/");
        List<Todo>? todos = await client.GetFromJsonAsync<List<Todo>>("todos/");
        List<Album>? albums = await client.GetFromJsonAsync<List<Album>>("albums/");

        // Create relational user entities
        foreach (var user in users)
        {
            user.Posts = posts.Where(e => e.UserId == user.Id).ToList();
            user.Todos = todos.Where(e => e.UserId == user.Id).ToList();
            user.Albums = albums.Where(e => e.UserId == user.Id).ToList();

            // Zero value ids so that they are assigned by identity count
            foreach (var post in user.Posts)
            {
                post.Id = 0;
            }
            foreach (var todo in user.Todos)
            {
                todo.Id = 0;
            }
            foreach (var album in user.Albums)
            {
                album.Id = 0;
            }

            user.Id = 0;
        }

        context.AddRange(users);
        context.SaveChanges();
    }
}
