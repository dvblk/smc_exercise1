﻿DELETE FROM [dbo].[Users];
DBCC CHECKIDENT ('Exercise1.dbo.Users', RESEED, 0);
DELETE FROM [dbo].[Addresses];
DBCC CHECKIDENT ('Exercise1.dbo.Addresses', RESEED, 0);
DELETE FROM [dbo].[Companies];
DBCC CHECKIDENT ('Exercise1.dbo.Companies', RESEED, 0);
DELETE FROM [dbo].[Albums];
DBCC CHECKIDENT ('Exercise1.dbo.Albums', RESEED, 0);
DELETE FROM [dbo].[Posts];
DBCC CHECKIDENT ('Exercise1.dbo.Posts', RESEED, 0);
DELETE FROM [dbo].[Todos];
DBCC CHECKIDENT ('Exercise1.dbo.Todos', RESEED, 0);

