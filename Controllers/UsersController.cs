﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using exercise_1.Models;

namespace exercise_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly Exercise1Context _context;

        public UsersController(Exercise1Context context)
        {
            _context = context;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
          if (_context.Users == null)
          {
              return NotFound();
          }

          return await (from u in _context.Users
                        join c in _context.Companies
                          on u.CompanyId equals c.Id
                        join a in _context.Addresses
                          on u.AddressId equals a.Id
                        select new User
                        {
                            Id = u.Id,
                            Name = u.Name,
                            Email = u.Email,
                            Phone = u.Phone,
                            Website = u.Website,
                            Company = c,
                            Address = a,
                        }).ToListAsync();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            if (_context.Users == null)
            {
                return NotFound();
            }

            var user = await (from u in _context.Users.Where(u => u.Id == id)
                              join c in _context.Companies
                                on u.CompanyId equals c.Id
                              join a in _context.Addresses
                                on u.AddressId equals a.Id
                              select new User
                              {
                                  Id = u.Id,
                                  Name = u.Name,
                                  Email = u.Email,
                                  Phone = u.Phone,
                                  Website = u.Website,
                                  Company = c,
                                  Address = a,
                                  Albums = _context.Albums.Where(e=> u.Id == e.UserId).ToList(),
                                  Posts = _context.Posts.Where(e => u.Id == e.UserId).ToList(),
                                  Todos = _context.Todos.Where(e => u.Id == e.UserId).ToList(),
                              }).FirstAsync();
            /* var user = await _context.Users.FindAsync(id);*/
            /*            var company = await _context.Companies
                            .Select(c => new Company
                            {
                                Id = c.Id,
                                Name = c.Name,
                                CatchPhrase = c.CatchPhrase,
                                Bs = c.Bs
                            })
                            .Where(c => c.Id == user.CompanyId).FirstAsync();

                        var address = await _context.Addresses
                            .Select(a => new Address
                            {
                                Id = a.Id,
                                Street = a.Street,
                                City = a.City,
                                Zipcode = a.Zipcode
                            })
                            .Where(c => c.Id == user.AddressId).FirstAsync();

                        user.Company = company;
                        user.Address = address;*/
            /*var user= _context.Users.FirstOrDefault(u=>u.Id==id);*/
            /*      var address = await _context.Addresses.FindAsync(user.Id);
                  var company = await _context.Companies.FindAsync(user.Id);
                  user.Address = address;
                  user.Company = company;*/

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            // TODO: any way to propegate updates in above statement?
            _context.Entry(user.Address).State = EntityState.Modified;
            _context.Entry(user.Company).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
          if (_context.Users == null)
          {
              return Problem("Entity set 'Exercise1Context.Users'  is null.");
          }
            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = user.Id }, user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            if (_context.Users == null)
            {
                return NotFound();
            }
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserExists(int id)
        {
            return (_context.Users?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
